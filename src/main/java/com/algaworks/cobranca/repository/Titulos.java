package com.algaworks.cobranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.algaworks.cobranca.model.Titulo;
//aula 2.4
//Titulo entidade que sera persistida no db entidade, tipo do ID da entidade no caso(private Long codigo;)
//spring date, ja tem uma implementacao generica
public interface Titulos extends JpaRepository<Titulo, Long> {

}
